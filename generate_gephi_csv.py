# -*- coding: utf-8 -*-
from __future__ import unicode_literals

"""
Ce fichier CSV permet d'extraire des informations de la base Neo4j pour visualisation dans Gephi.
"""

#########################################
################# Init #################
#########################################
### Import de différent modules
import neo4j.v1 as neo4j    # Lib Neo4j
import ConfigParser                             # Configuration file parser
import sys                                      # System call

### Variable diverses
# Fichier de conf
config = 'config'
# Fichiers CSV de sortie
entreprises_file = "extract_gephi/entreprises.csv"
edges_file = "extract_gephi/edges.csv"
benef_file = "extract_gephi/benef.csv"

### Create a SafeConfigParser
parser = ConfigParser.SafeConfigParser(allow_no_value=True)
parser.optionxform=str
try:
    parser.read(config)
    ### Extract vars
    host = parser.get('neo4j','host')
    user = parser.get('neo4j','user')
    password = parser.get('neo4j','password')
except ConfigParser.ParsingError as e:
    print 'Error while reading conf file\n' + str(e)
    sys.exit(1)
except (ConfigParser.NoSectionError,ConfigParser.NoOptionError) as e:
    print 'Error : configuration file seems not valid\n' + str(e)
    sys.exit(1)

### Requete de recuperation des informations
##### ATTENTION #####
"""
Cette requete doit recupere les resultats sous la forme [Entreprise,Relation,Beneficiaire] dans la base Neo4j. Pour cela, la requete
DOIT se terminer par "RETURN entreprise,link,benef".
"""
# On filtre les avantages de 1000 euros ou plus.
request = 'MATCH (s)-[r]->(e) WHERE r.montant > 999 RETURN s,r,e'

#########################################
############### Fonctions ###############
#########################################
def clean_entry(entry):
    """
    Fonction qui nettoie une ligne (sous contenue dans un dict Python) des caracteres ennuyeux (virgule pour le CSV par exemple).
    Input :
        - entry (dict) : entree a nettoyer.
    """
    for key in entry:
        if isinstance(entry[key],unicode) or isinstance(entry[key],str):
            entry[key] = entry[key].replace(',',' ') # Escape ,
    return entry

def format_node(raw_node):
    """
    Formate un noeud de la base Neo4j en un dict Python.
    """
    node = raw_node.properties
    node['id'] = raw_node.id
    return clean_entry(node)

def format_link(raw_link):
    """
    Formate un lien de la base Neo4j en un dict Python.
    """
    link = raw_link.properties
    link['id'] = raw_link.id
    link['start'] = raw_link.start
    link['end'] = raw_link.end
    link['type'] = raw_link.type
    return clean_entry(link)

def write_csv(entreprises_nodes, links, ends_nodes, totaux=None):
    """
    Ecrit les fichiers CSV contenant les entreprises et beneficiaires (noeuds) et les relations (liens) qui seront mis dans Gephi.
    Input :
        - entreprises_nodes (list) : liste des dict entreprises
        - links (list) : liste des liens
        - ends_nodes (list) : liste des dict beneficiaires
        - totaux (dict) (optionnel): dict contenant, pour chaque entreprise ID, le total de ses transactions
    """
    ### Entreprises
    if totaux is not None:
        entreprises_str = "Id,Node_Type,Nom_Entreprise,Total\n"
        for entreprise in entreprises_nodes:
            entreprises_str += str(entreprise['id']) + ',Entreprise,' + entreprise['nom'] + ',' + str(totaux[entreprise['id']]) + '\n'
    else:
        entreprises_str = "Id,Node_Type,Nom_Entreprise\n"
        for entreprise in entreprises_nodes:
            entreprises_str += str(entreprise['id']) + ',Entreprise,' + entreprise['nom'] + '\n'
    ### Write entreprises
    fd = open(entreprises_file, 'w')
    fd.write(entreprises_str.encode('utf-8'))
    fd.close()

    ### Benef
    benef_str = "Id,Node_Type,Nom,Prenom,Type\n"
    for benef in ends_nodes:
        benef_str += str(benef['id']) + ',Beneficiaire,' + benef['nom'] + ',' + benef['prenom'] + ',' + benef['type'] + '\n'
    ### Write benef
    fd = open(benef_file, 'w')
    fd.write(benef_str.encode('utf-8'))
    fd.close()

    ### Edges
    edges_str = "Source,Target,Date,Montant,Nature,Adresse,CodePostal,Ville,Pays,Qualification,Specialite,Titre,Denomination,Objet,Etablissement,EtablissementCodePostal,EtablissementVille\n"
    for edge in links:
        edges_str += str(edge['start']) + ',' + str(edge['end']) + ',' + str(edge['date']) + ',' + str(edge['montant']) + ',' + edge['nature'] + ',' + edge['adresse'] + ',' + edge['benef_code_postal'] + ',' + edge['benef_ville'] + ',' + edge['benef_ville'] + ',' + edge['benef_code_pays'] + ',' + edge['qualification'] + ',' + edge['code_specialite'] + ',' + edge['code_titre'] + ',' + edge['denomination_sociale'] + ',' + edge['objet_social'] + ',' + edge['etablissement'] + ',' + edge['etablissement_code_postal'] + ',' + edge['etablissement_ville'] + '\n'
    ### Write links
    fd = open(edges_file, 'w')
    fd.write(edges_str.encode('utf-8'))
    fd.close()

#########################################
################ Script #################
#########################################
### Connexion à la BDD Neo4j
driver = neo4j.GraphDatabase.driver("bolt://"+host, auth=neo4j.basic_auth(user, password))
session = driver.session()

### On execute la requete
print "Request data from DB"
result = session.run(request.encode('utf-8'))
print "Load data"
raw_data = [record for record in result]
### Close session and commit result
session.close()

### Process data
print "Process data"
entreprise_nodes = []
links = []
end_nodes = []
total_by_id = {}
for record in raw_data:
    ## Recuperation de l'entreprise
    entreprise_node = format_node(record.items()[0][1])
    # Ajout dans la liste des entreprises si elle n'y est pas
    if not entreprise_node['id'] in [e['id'] for e in entreprise_nodes]:
        total_by_id[entreprise_node['id']] = 0
        entreprise_nodes.append(entreprise_node)

    ## Recuperation de la relation
    link = format_link(record.items()[1][1])
    links.append(link)

    ## Recuperation du beneficiaire
    end_node = format_node(record.items()[2][1])
    # Ajout dans la liste des beneficiaires si elle n'y est pas
    if not end_node['id'] in [e['id'] for e in end_nodes]:
        end_nodes.append(end_node)

    ## Ajout du montant de l'avantage au total de l'entreprise
    total_by_id[entreprise_node['id']] += link['montant']

### On ecrit les fichiers CSV
print "Write CSV files"
write_csv(entreprise_nodes, links, end_nodes,total_by_id)
