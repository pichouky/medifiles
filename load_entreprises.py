# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#########################################
################# Init #################
#########################################
### Import de différent modules
from neo4j.v1 import GraphDatabase, basic_auth  # Connecteur avec la BDD Neo4j
import unicodecsv as csv                        # Manipulation des CSV
import time                                     # Mesure de temps
import ConfigParser                             # Configuration file parser
import sys                                      # System call

### Variable diverses
# Fichier de conf
config = 'config'

### Create a SafeConfigParser
parser = ConfigParser.SafeConfigParser(allow_no_value=True)
parser.optionxform=str
try:
    parser.read(config)
    ### Extract vars
    host = parser.get('neo4j','host')
    user = parser.get('neo4j','user')
    password = parser.get('neo4j','password')
    csv_file = parser.get('entreprises','csv_file')
except ConfigParser.ParsingError as e:
    print 'Error while reading conf file\n' + str(e)
    sys.exit(1)
except (ConfigParser.NoSectionError,ConfigParser.NoOptionError) as e:
    print 'Error : configuration file seems not valid\n' + str(e)
    sys.exit(1)

#########################################
############ Load entreprises ###########
#########################################

### Connexion à la BDD Neo4j
driver = GraphDatabase.driver("bolt://"+host, auth=basic_auth(user, password))
session = driver.session() # L'object session permet de faire des requête à la base

### On garde en mémoire le moment de démarrage du script, pour mesurer son temps d'execution
start = time.time()

### On ouvre le fichier CSV et on le charge dans un tableau qui va s'appeller data
fd = open(csv_file,'rb')
data = list(csv.reader(fd,encoding='utf-8'))
fd.close()

### On parcourt toutes les lignes du CSV (pas la première, c'est l'entete)
for entry in data[1:]:
    ### On echappe les caractères un peu chiants
    for i in range(len(entry)):
        entry[i] = entry[i].replace('\\','\\\\') # Escape \
        entry[i] = entry[i].replace('"','\\"') # Escape "

    ## On prépare une requete pour ajouter l'entreprise dans la base, c'est une chaine de caractère
    req = 'CREATE (e:Entreprise {'
    req += 'identifiant: "'+entry[0]+'"'
    req += ',nom: "'+entry[5]+'"'
    req += ',code_pays: "'+entry[1][1:len(entry[1])-1]+'"'
    req += ',code_secteur: "'+entry[3][1:len(entry[1])-1]+'"'
    req += ',adresse: "'+entry[6]+' '+entry[7]+' '+entry[8]+' '+entry[9]+'"'
    req += ',code_postal: "'+entry[10]+'"'
    req += ',ville: "'+entry[11].lower()+'"'
    req += '})'
    ## On soumet la requete à la BDD
    session.run(req.encode('utf-8'))

### Quand c'est fini, on ferme la connexion pour appliquer les requetes
session.close()
print 'Load all entreprises nodes in '+str(round(time.time()-start,2))+' seconds.'
