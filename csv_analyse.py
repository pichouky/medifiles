# -*- coding: utf-8 -*-
from __future__ import unicode_literals

"""
Ce script permet d'analyser rapidement les lignes des fichiers CSV avantages. Il est utile pour faire quelques stats simples et rapides sur les data.
Les avantages doivent se trouver dans plusieurs fichiers CSV (pour ne pas charger d'un coup un énorme fichier) présents dans le répertoire raw_data/split_files.
Chaque ligne avantage est parcourue par le script.
"""

#########################################
################# Init #################
#########################################
### Import de différent modules
import unicodecsv as csv    # Manipulation des CSV
import os                   # Appel systeme
import unicodedata          # Gestion de l'unicode
import counter              # Compteur pour des stats
import tabulate             # Affichage propre de tableau dans le terminal
import re                   # Gestion des regex

### Variable diverses
# Nom du dossier contenant l'ensemble des petits fichiers CSV
split_csv_folder = "raw_data/split_files"

def match(regex,word):
    """
    Compare une chaine de caracteres avec une regex.
    Input:
        - regex (str): Regex
        - word (str): Chaine a analyser
    Output:
        - (Boolean) : True si le chaine correspond a la regex, False sinon
    """
    r = re.compile(regex)
    m = r.match(word)
    if m is None:
        return False
    else:
        return True

def normalize_special_char(string):
    """
    Normalise les accents et autres caracteres non-ASCII.
    """
    return unicodedata.normalize('NFKD',string).encode('ASCII','ignore')

#########################################
################ Analyse ################
#########################################
### On récupère la liste des fichiers CSV qu'il faudra analyser dans un tableau csv_files
cmd = os.popen("ls "+split_csv_folder)
csv_files = [line.rstrip('\n') for line in cmd]

# Initialisation compteur pour stats
c = counter.Counter()

##### On parcourt les fichiers CSV #####
for file in csv_files:

    ### On ouvre le fichier CSV et on le charge dans un tableau qui va s'appeller data
    fd = open(split_csv_folder+"/"+file,'rb')
    data = list(csv.reader(fd,encoding='utf-8',delimiter=str(";")))
    fd.close()

    """
    Analyse ligne par ligne
    Le code qui suit est l'analyse que l'on souhaite faire sur chaque ligne
    Ici, en exemple, on compte les avantages inférieurs et supérieurs à 100 euros.
    """
    ### On parcourt les lignes du CSV
    # La variable entry est un tableau qui contient les entrées d'une ligne du CSV (voir la doc)
    for entry in data:
        # Ici on compte les cases avantages inférieurs (et supérieurs donc) à 100 euros
        if int(entry[33]) < 100:
            c['inf'] += 1
        else:
            c['sup'] += 1

#########################################
############### Affichage ###############
#########################################
### Affichage sous forme de tableau sur STDIN
stats_table = []
for val,nb in c.most_common():
    stats_table.append([val,nb])
print tabulate.tabulate(stats_table,["Valeur", "nombre"],tablefmt="grid")

### Ecriture du tableau dans un fichier (utile si non-ASCII)
#fd = open("tmp.log",'w')
#fd.write(tabulate.tabulate(stats_table,["Valeur", "nombre"],tablefmt="grid").encode('utf-8'))
#fd.close()
