# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#########################################
################# Init #################
#########################################
### Import de différent modules
from neo4j.v1 import GraphDatabase, basic_auth  # Connecteur avec la BDD Neo4j
import unicodecsv as csv                        # Manipulation des CSV
import time                                     # Mesure de temps
from datetime import datetime                   # Manipulation des dates et timestamps
import os                                       # Appel systeme
import unicodedata                              # Gestion unicode
import re                                       # Gestion regex
import ConfigParser                             # Configuration file parser
import sys                                      # System call

### Variable diverses
# Fichier de conf
config = 'config'
# File to analyse
split_file = "raw_data/split_files/zdhm"

### Create a SafeConfigParser
parser = ConfigParser.SafeConfigParser(allow_no_value=True)
parser.optionxform=str
try:
    parser.read(config)
    ### Extract vars
    host = parser.get('neo4j','host')
    user = parser.get('neo4j','user')
    password = parser.get('neo4j','password')
    error_csv_file = parser.get('avantages','error_csv_file')
except ConfigParser.ParsingError as e:
    print 'Error while reading conf file\n' + str(e)
    sys.exit(1)
except (ConfigParser.NoSectionError,ConfigParser.NoOptionError) as e:
    print 'Error : configuration file seems not valid\n' + str(e)
    sys.exit(1)

#########################################
############### Functions ###############
#########################################
def match(regex,word):
    """
    Compare a string with a regex.
    Input:
        - regex (str): string of the regex
        - word (str): string to analyze
    Output:
        - (Boolean) : True if the regex match, False otherwise
    """
    r = re.compile(regex)
    m = r.match(word)
    if m is None:
        return False
    else:
        return True

def add_invalids(invalids):
    """
    Ajoute les lignes invalides dans un csv dédié
    Input:
        - invalids (tab) : Tableau contenant les lignes (des tableaux) à ajouter
    """
    if not invalids:
        return
    fd = open(error_csv_file,"a")
    for entry in invalids:
        line = '"' + '";"'.join(entry) + '"'
        line += "\n"
        fd.write(line.encode('utf-8'))
    fd.close()

def normalize_special_char(string):
    """
    Normalise les accents et autres caracteres non-ASCII.
    """
    return unicodedata.normalize('NFKD',string).encode('ASCII','ignore')

#########################################
############# Load avantages ############
#########################################
### Connexion à la BDD Neo4j
driver = GraphDatabase.driver("bolt://"+host, auth=basic_auth(user, password))

##### On parcourt les fichiers CSV #####
session = driver.session()
### On ouvre le fichier CSV et on le charge dans un tableau qui va s'appeller data
fd = open(split_file,'rb')
data = list(csv.reader(fd,encoding='utf-8',delimiter=str(";")))
fd.close()

### On met les entrées invalides dans un tableau
invalids = []
### On parcourt les lignes du CSV
# La variable entry est un tableau qui contient les entrées d'une ligne du CSV (voir la doc)
for entry in data:
    ### On echappe les caractères un peu chiants
    for i in range(len(entry)):
        entry[i] = entry[i].replace('\\','\\\\') # Escape \
        entry[i] = entry[i].replace('"','\\"') # Escape "

    ## Analyse si la ligne est valide
    # Code postal
    if (not match("^[0-9]{5}$",entry[14]) and entry[14] != '') or (not match("^[0-9]{5}$",entry[27]) and entry[27] != ''):
        invalids.append(entry)
        continue

    ### On ignore les montants < 100€
    if int(entry[33]) < 100:
        invalids.append(entry)
        continue
    # Test exist
    result=session.run('MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WHERE p.nom = "'+normalize_special_char(entry[6].lower())+'" AND p.prenom = "'+normalize_special_char(entry[7].lower())+'" AND e.identifiant = "'+entry[0]+'" AND c.montant='+str(entry[33])+' RETURN p.nom AS nom LIMIT 1'.encode('utf-8'))
    count = [e['nom'] for e in result]
    if count != 0:
        print "Already exists"
	continue
    ## On récupère le type du bénéficiaire
    type = entry[4][1:len(entry[4])-1]
    if type == "PRS": ## Pro de santé
        ## On extrait les données
        # On recupere le nom et le prenom si ils sont valides
        nom = normalize_special_char(entry[6].lower())
        prenom = normalize_special_char(entry[7].lower())
        if not match('^[a-z\-\ ]*$',nom) or not match('^[a-z\-\ ]*$',prenom):
            invalids.append(entry)
            continue
        # On transforme les - de prenom en espace pour uniformiser
        nom = nom.replace('-',' ')
        prenom = prenom.replace('-',' ')

        # On recupere son type d'ID
        id_type_code = entry[23][1:len(entry[23])-1]
        # Si il y a un ID (si de type RPPS) on recupère l'ID. Sinon on met à None.
        if id_type_code == "RPPS":
            id_benef = entry[25]
        else:
            id_benef = None

        ## On regarde si le noeud existe deja
        result = session.run('MATCH (p:Personne {nom:"'+nom+'",prenom:"'+prenom+'"}) RETURN p.nom AS nom LIMIT 1'.encode('utf-8'))
        count = [e['nom'] for e in result]
        if len(count) == 0:
            # Si il n'existe pas, on prepare une requete de creation
            person_req = 'CREATE (p:Personne {'
            person_req += 'nom: "'+nom+'"'
            person_req += ',prenom: "'+prenom+'"'
            person_req += ',id_type_code: "'+id_type_code+'"'
            person_req += ',id_benef: "'+unicode(id_benef)+'"'
            person_req += ',type: "'+type+'"'
            person_req += '}) WITH p'
        else:
            # Si il existe déjà on prepare une requete pour le recuperer
            person_req = 'MATCH (p:Personne {nom:"'+nom+'",prenom:"'+prenom+'"})'

        ## Creation du lien avec l'entreprise
        # On recupère des infos sur le lien
        timestamp = int(time.mktime(datetime.strptime(entry[32], '%d/%m/%Y').timetuple())) # Date en timestamp
        montant = int(entry[33]) # Montant
        type_av = entry[34] # Type avantage
        qualite = entry[9] # Qualite
        adresse = entry[10]+' '+entry[11]+' '+entry[12]+' '+entry[13]
        benef_code_postal = entry[14]
        benef_ville = entry[15]
        benef_code_pays = entry[16][1:-1]
        code_titre = entry[18][1:-1]
        code_specialite = entry[20]
        qualification = entry[22]
        type_id = entry[24]
        identifiant = entry[25]
        etablissement = entry[26]
        etablissement_code_postal = entry[27]
        etablissement_ville = entry[28]
        denomination_sociale = entry[29]
        objet_social = entry[30]

        ## On prépare la requête pour récupérer l'entreprise
        entreprise_req = 'MATCH (e:Entreprise {identifiant:"'+entry[0]+'"})'

        ## On prepare la requete de creation du lien
        ## On prepare la requete de creation du lien
        link_req = 'CREATE (e)-[:GIVE {date: '+unicode(timestamp)+''
        link_req += ',montant: '+unicode(montant)+''
        link_req += ',nature: "'+type_av+'"'
        link_req += ',adresse: "'+adresse+'"'
        link_req += ',benef_code_postal: "'+benef_code_postal+'"'
        link_req += ',benef_ville: "'+benef_ville+'"'
        link_req += ',benef_code_pays: "'+benef_code_pays+'"'
        link_req += ',code_titre: "'+code_titre+'"'
        link_req += ',code_specialite: "'+code_specialite+'"'
        link_req += ',qualification: "'+qualification+'"'
        link_req += ',type_id: "'+type_id+'"'
        link_req += ',identifiant: "'+identifiant+'"'
        link_req += ',etablissement: "'+etablissement+'"'
        link_req += ',etablissement_code_postal: "'+etablissement_code_postal+'"'
        link_req += ',etablissement_ville: "'+etablissement_ville+'"'
        link_req += ',denomination_sociale: "'+denomination_sociale+'"'
        link_req += ',objet_social: "'+objet_social+'"'
        link_req += '}]->(p)'

        ## On lance la requête globale
        session.run((person_req+'\n'+entreprise_req+'\n'+link_req).encode('utf-8'))
    elif type == "ETU": ## Etudiant
        ## On extrait les données
        # On recupere le nom et le prenom si ils sont valides
        nom = entry[6].lower()
        prenom = entry[7].lower()
        if not match('^[a-z\-\ ]*$',nom) or not match('^[a-z\-\ ]*$',prenom):
            invalids.append(entry)
            continue
        # On transforme les - de prenom en espace pour uniformiser
        nom = nom.replace('-',' ')
        prenom = prenom.replace('-',' ')

        # On recupere son type d'ID
        id_type_code = entry[23][1:-1]
        # Si il y a un ID (si de type RPPS) on recupère l'ID. Sinon on met à None.
        if id_type_code == "RPPS":
            id_benef = entry[25]
        else:
            id_benef = None

        ## On regarde si le noeud existe deja
        result = session.run('MATCH (p:Personne {nom:"'+nom+'",prenom:"'+prenom+'"}) RETURN p.nom AS nom LIMIT 1'.encode('utf-8'))
        count = [e['nom'] for e in result]
        if len(count) == 0:
            # Si il n'existe pas, on prepare une requete de creation
            person_req = 'CREATE (p:Personne {'
            person_req += 'nom: "'+nom+'"'
            person_req += ',prenom: "'+prenom+'"'
            person_req += ',id_type_code: "'+id_type_code+'"'
            person_req += ',id_benef: "'+unicode(id_benef)+'"'
            person_req += ',type: "'+type+'"'
            person_req += '}) WITH p'
        else:
            # Si il existe déjà on prepare une requete pour le recuperer
            person_req = 'MATCH (p:Personne {nom:"'+nom+'",prenom:"'+prenom+'"})'

        ## Creation du lien avec l'entreprise
        # On recupère des infos sur le lien
        timestamp = int(time.mktime(datetime.strptime(entry[32], '%d/%m/%Y').timetuple())) # Date en timestamp
        montant = int(entry[33]) # Montant
        type_av = entry[34] # Type avantage
        qualite = entry[9] # Qualite
        adresse = entry[10]+' '+entry[11]+' '+entry[12]+' '+entry[13]
        benef_code_postal = entry[14]
        benef_ville = entry[15]
        benef_code_pays = entry[16][1:-1]
        code_titre = entry[18][1:-1]
        code_specialite = entry[20]
        qualification = entry[22]
        type_id = entry[24]
        identifiant = entry[25]
        etablissement = entry[26]
        etablissement_code_postal = entry[27]
        etablissement_ville = entry[28]
        denomination_sociale = entry[29]
        objet_social = entry[30]

        ## On prépare la requête pour récupérer l'entreprise
        entreprise_req = 'MATCH (e:Entreprise {identifiant:"'+entry[0]+'"})'

        ## On prepare la requete de creation du lien
        link_req = 'CREATE (e)-[:GIVE {date: '+unicode(timestamp)+''
        link_req += ',montant: '+unicode(montant)+''
        link_req += ',nature: "'+type_av+'"'
        link_req += ',adresse: "'+adresse+'"'
        link_req += ',benef_code_postal: "'+benef_code_postal+'"'
        link_req += ',benef_ville: "'+benef_ville+'"'
        link_req += ',benef_code_pays: "'+benef_code_pays+'"'
        link_req += ',code_titre: "'+code_titre+'"'
        link_req += ',code_specialite: "'+code_specialite+'"'
        link_req += ',qualification: "'+qualification+'"'
        link_req += ',type_id: "'+type_id+'"'
        link_req += ',identifiant: "'+identifiant+'"'
        link_req += ',etablissement: "'+etablissement+'"'
        link_req += ',etablissement_code_postal: "'+etablissement_code_postal+'"'
        link_req += ',etablissement_ville: "'+etablissement_ville+'"'
        link_req += ',denomination_sociale: "'+denomination_sociale+'"'
        link_req += ',objet_social: "'+objet_social+'"'
        link_req += '}]->(p)'

        ## On lance la requête globale
        session.run((person_req+'\n'+entreprise_req+'\n'+link_req).encode('utf-8'))
    else:
        invalids.append(entry)
        continue
# Add invalids entries to CSV
add_invalids(invalids)

### Commit result
session.close()
