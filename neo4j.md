# Neo4j

La base de donnée Neo4j étant orientée graphe, on peut facilement manipuler les noeuds et liens de notre grand graphe. Le langage utilisé par Neo4j est *Cypher*.

## Déploiement
La base de donnée *Neo4j* est assez simple à déployer avec Docker. Nous avons installé *Docker* et *docker-compose* (suivre la doc sur https://docs.docker.com/) et nous avons écrit le fichier `docker-compose.yml` suivant :
```
neo4j:
    mem_limit: 4096m
    image: neo4j:3.0
    volumes:
        - /root/docker/volumes/neo4j/data:/data
    environment:
        - NEO4J_AUTH=MyUsername/MyPassword
    ports:
        - "7473:7473"
        - "7474:7474"
        - "7687:7687"
```

Il suffit de créer un répertoire pour accueillir les données puis de lancer le conteneur (on utilise l'image officielle proposée sur le Docker Hub).
```
mkdir -p /root/docker/volumes/neo4j/data
docker-compose up -d neo4j
```

### Backup/Restore
La base de donnée Neo4j est stockée dans le répertoire `graph.db` des données. Il est donc assez simple de sauvegarder le tout de la manière suivante:
```
cd /root/docker/volumes/neo4j/data/databases
tar -czf neo4j.backup.tar.gz graph.db/
```

Pour la restaurer, il suffit de remplacer le répertoire `graph.db` (avec Neo4j arrêté):
```
cd /root/docker/volumes/neo4j/data/databases
rm -rf graph.db
tar -xzf neo4j.backup.tar.gz
```

## Prise en main de Cypher

### Prise en Base
Le langage *Cypher* ressemble assez à du SQL basique, mais permet de manipuler des relations dans un graphe.
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) RETURN e,c,p LIMIT 20
```
Cette requête peut-être découpée de la manière suivante :
 - `MATCH (e:Entreprise)-[c:GIVE]->(p:Personne)` indique que l'on veut récupérer toutes les relations *GIVE* associée à un noeud *Entreprise* vers un noeud *Personne*. Les variables `e`, `c` et `p` contiennent, pour chaque relation, l'entreprise, le lien et la personne.
 - `RETURN e,c,p` retourne les variables `e`,`c` et `p` de chaque relations.
 - `LIMIT 20` limite à 20 le nombre de résultats (**très** important sinon la requête essaye de retourner toute la base).

On peut aussi ajouter une condition à la requête. Ici on récupère uniquement les relations où la personne porte un nom et prénom bien défini.
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WHERE p.nom = 'doe' AND p.prenom = 'john' RETURN e,c,p
```

Bien évidemment la condition peut s'appliquer à tout les attributs de chacun des objets impliqués dans la relation. Par exemple ici on veut toutes les transactions supérieures à 10000€.
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WHERE c.montant > 10000 RETURN e,c,p
```
### Requêtes avancées

#### Regex
*Cypher* donne la possibilité d'utiliser des expressions régulières dans les requêtes. Par exemple si on veut extraire tout les avantages qui sont déclarés dans le département de l'Ain, on appliquera une expression régulière sur le code postal.
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WHERE c.benef_code_postal=~'01[0-9]{3}'  RETURN e,c,p
```

#### Sommes
On peut aussi isoler des attributs sur la totalité des résultats, puis réaliser un traitement dessus. Par exemle on veut récupérer, pour chaque entreprise, la liste des avantages, et en réaliser la somme.
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WITH DISTINCT e.nom as ent,SUM(c.montant) as amount ORDER BY SUM(c.montant) DESC RETURN ent,amount
```
On obtient ainsi le classement des entreprises qui donnent le plus.

### Requêtes intéressantes
Une liste des requêtes intéressantes que nous avons utilisées au cours de notre analyse.
- Classement des entreprises qui font le plus de cadeaux.
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WITH DISTINCT e.nom as ent,COUNT(c) as nb ORDER by nb DESC RETURN ent,nb
```

- Classement des entreprises qui donnent le plus (en euros)
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WITH DISTINCT e.nom as ent,SUM(c.montant) as amount ORDER BY SUM(c.montant) DESC RETURN ent,amount
```

- Classement des entreprises qui donnent le plus dans un secteur d'activité donné (voir les codes de spécialités dans la documentation des CSV sources)
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WHERE c.code_specialite='[SM21]'  WITH DISTINCT e.nom as ent,SUM(c.montant) as amount  ORDER BY SUM(c.montant) DESC RETURN ent,amount
```

- Classement des personnes qui touche le plus d'argents en cadeaux
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WITH DISTINCT p.nom + '  ' + p.prenom as perso, SUM(c.montant) as amount ORDER BY SUM(c.montant) DESC RETURN perso,amount LIMIT 20
```

- Classement des personnes qui ont des relations avec le plus grand nombre d'entreprises différentes
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WITH DISTINCT p as pers,e as ent RETURN pers.nom+' '+pers.prenom,COUNT(ent) ORDER BY COUNT(ent) DESC LIMIT 20
```

- Classement des personnes touchant le plus grand nombre de cadeaux
```
MATCH (e:Entreprise)-[c:GIVE]->(p:Personne) WITH DISTINCT p.nom+' '+p.prenom  as perso,COUNT(c) as nb order by nb DESC RETURN perso,nb limit 20
```
