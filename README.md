# Medifiles

## Overview
Dans le cadre de l'article L.1453-1 du code de la santé publique, les entreprises produisant ou commercialisant des produits médicaux ou assurant des prestations associées à ces produits sont tenues de rendre publique l'existence des conventions qu'elles concluent avec différents acteurs de la santé (médecins, étudiants, associations, etc.).

Les avantages perçus par des acteurs du domaine de la santé sont accessibles sur le site https://www.transparence.sante.gouv.fr/. Ce site permet de faire des recherches à l'aide d'un formulaire, suivant un certain nombre de critères. Cependant ceci ne permet pas de répondre facilement à certaines questions et ne permet pas d'avoir une vision d'ensemble des données en question. Dans le cadre de la mission Etalab, les données sont proposées sous la forme de fichiers CSV sur le site https://www.data.gouv.fr/fr/datasets/transparence-sante-1/.

Ce jeu de scripts vise à analyser rapidement et à extraire les données de ces fichiers CSV pour les charger dans une base de donnée Neo4j.

## Préparation

### Architecture du projet
Pour utiliser les projet, il convient de récupérer les scripts ainsi que les données CSV.

On clone et prépare le repo Git.
```
git clone git@gitlab.utc.fr:pichouky/medifiles.git
cd medifiles
mkdir -p raw_data/split_files
mkdir -p raw_data/invalids_files
mkdir -p extract_gephi
```

On récupère les fichiers CSV sur le site Transparence-santé. Les données ne semblent actuellement accessibles que pour certains User-Agent, on simule donc un semblant de User-Agent Firefox.
```
wget --user-agent="Mozilla/5.0 Firefox/45.0" https://www.transparence.sante.gouv.fr/exports-etalab/exports-etalab.zip
unzip exports-etalab.zip -d raw_data
rm exports-etalab.zip
mv raw_data/declaration_avantage_*.csv raw_data/avantages.csv
mv raw_data/declaration_convention_*.csv raw_data/conventions.csv
mv raw_data/entreprise_*.csv raw_data/entreprises.csv
```

### Environnement technique
Tout les scripts sont développés en Python (version 2.7.13) et fonctionnent sous Linux. Quelques modules Python doivent être installés, ceux-ci sont disponibles dans la fichier `requirements.txt`. Pour avoir un environnement propre, il est recommandé d'installer `virtualenv` :
```
pip install --user virtualenv
```
On se place ensuite à la racine du projet (`cd medifiles`) et on créé un environnement virtuel, on l'active et on installe les dépendances
```
virtualenv ./env_medifiles
source ./env_medifiles/bin/activate
pip install -r requirements.txt
```
Pour quitter l'environnement virtuel, on utilise simplement la commande `deactivate`.

### Fichiers de données
Les données sont fournit sous la forme de 3 fichiers CSV ainsi que quelques fichiers de présentation rapide du format des données.  

Un premier fichier contient la liste des entreprises présentes dans la base. Le second fichier contient la liste des avantages (informations sur les bénéficiaires, l'entreprise et les montants), et le dernier fichier contient la liste des conventions.
Le projet ne permet actuellement de traiter que les avantages, les informations sur les conventions n'étant pas complète (mise à jour possible en Avril 2017).

Le fichier contenant les avantages est **très** volumineux (plusieurs millions de lignes) et ne peux donc pas être manipulé par un script en une seule fois. On va donc le découper en plusieurs fichiers CSV plus petits.
```
split -l 2000 raw_data/avantages.csv raw_data/split_files/
echo "$(tail -n +2 raw_data/split_files/aa)" > raw_data/split_files/aa
```

## Génération d'une base de donnée Neo4j

Les scripts `load_entreprises.py` et `load_avantages.py` permettent de charger les informations des fichiers CSV dans une base de donnée Neo4j. L'utilisation de Neo4j et la manipulation de la base sont rapidement décrit dans le fichier [neo4j.md](neo4j.md).

### Fichier de configuration
Les différents scripts de se projets nécessitent de renseigner quelques variables de configuration (login/password de la base Neo4j, fichiers des données CSV, etc.).  
Pour cela on copie le fichier d'exemple avec `cp config_example config` puis on modifie les variables suivantes dans le fichier `config` :
- Section `[neo4j]`
  - `host` : hostname ou IP vers la base de donnée Neo4j
  - `user` : utilisateur de la base
  - `password` : mot de passe pour l'utilisateur
- Section `[avantages]`
  - `split_csv_folder` : chemin vers le dossier contenant les fichiers CSV des avantages
  - `error_csv_file` : chemin vers le dossier contenant les fichiers CSV des avantages
- Section `[entreprises]`
  - `csv_file` : chemin vers le fichier CSV contenant la liste des entreprises

### Entreprises
Le script `load_entreprises.py` est le premier à utiliser et va créer l'ensemble des entreprises dans la base de données. Il est possible de lancer le script avec la commande `python load_entreprises.py`.

### Avantages
Le script `load_avantages.py` va créer l'ensemble des liens et des bénéficiaires dans la base de données. Il est ensuite possible de lancer le script avec la commande `python load_avantages.py`.

## Analyse des données
Le script `csv_analyse.py` permet de réaliser une analyse rapide des données directement depuis les fichiers CSV des avantages. Ce script est simplement une "coquille" qui parcourt l'ensemble des lignes avantages des fichiers CSV. Il est possible de rajouter un traitement particulier pour, par exemple, faire des statistiques sur les spécialités les plus représentées.  
C'est donc un script personnalisable, qui propose simplement le code permettant de parcourt les fichiers CSV, ainsi que quelques fonctions et morceaux de code utiles.

## Export pour Gephi

Il est possible d'extraire une partie de la base de données pour la visualiser dans Gephi. Pour cela, il suffit de générer des fichiers CSV contenant les noeuds et les liens pour Gephi, en respectant le format nécessaire.

Le script `generate_gephi_csv.py` permet d'extraire les données d'une requête et de générer des tables de noeuds `entreprises.csv` et `benef.csv` ainsi qu'une table de liens `edges.csv`.
Pour fonctionner le script a besoin des informations suivantes, à ajouter en début de code :
- `request` : requête de recupération des informations dans la base Neo4j.
La requête doit **impérativement** retourner les résultats sous la forme d'un tableau [Entreprise,Relation,Beneficiaire] pour que le script fonctionne correctement. La requête doit donc impérativement faire un retour de la forme `RETURN entreprise,link,benef`.

Le fichier `entreprises.csv` contient l'ensemble des entreprises (id, nom, montant total versé) retournées par la requête.  
Le fichier `benef.csv` contient l'ensemble des bénéficiaires (id, nom, prénom, profession) retournés par la requête.  
Le fichier `edges.csv` contient l'ensemble des informations de liens entre entreprises et bénéficiaires.
